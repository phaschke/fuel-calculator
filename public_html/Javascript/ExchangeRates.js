/*export function getExchangeRate(sCurrency) {

  return getAndValidateExchangeRates().rates[sCurrency];
}*/

export function getExchangeRate(sCurrency) {

  let oExchangeRates = getExchangeRates();

  if(!oExchangeRates || !validateExchangeRateDate(oExchangeRates)) {
    initalizeExchangeRates(function(oExchangeRates) {

      console.log("exchangerates");
      console.log(oExchangeRates);

      return oExchangeRates.rates[sCurrency];
    });
  }

  return oExchangeRates.rates[sCurrency];

  //if(!validateExchangeRateDate(oExchangeRate)) return initalizeExchangeRates();

  //return oExchangeRate;

}

function getExchangeRates() {

  let sExchangeRate = FuelCalculator.Storage.getData("ExchangeRates");

  try {
    let oExchangeRate = JSON.parse(sExchangeRate);
    return oExchangeRate;

  } catch (e) {

    return null;
  }
}

function validateExchangeRateDate(oExchangeRate) {

  let sDate = oExchangeRate.date;
  if(!sDate) return false;

  return compareDateToToday(sDate);

}

function compareDateToToday(sDate) {

  let aDate = sDate.split("-");

  let oTodayDate = new Date();

  let sDay = String(oTodayDate.getDate()).padStart(2, '0');
  let sMonth = String(oTodayDate.getMonth() + 1).padStart(2, '0');
  let sYear = oTodayDate.getFullYear();

  if(aDate[2] != sDay) return false;
  if(aDate[1] != sMonth) return false;
  if(aDate[0] != sYear) return false;

  return true;

}


function initalizeExchangeRates(fCallback) {

  console.log("initalizeExchangeRates");

  fetch('https://api.exchangeratesapi.io/latest?base=USD')
  .then(response => response.json())
  .then(data => {
    FuelCalculator.Storage.saveData("ExchangeRates", JSON.stringify(data));

    console.log(data);

    //return data;
    if (fCallback && typeof(fCallback) === "function") {
      fCallback(data);
    }
  });

  /*let oExchangeRate = {
    "rates": {
        "CAD": 1.2802600867,
        "HKD": 7.7528342781,
        "ISK": 130.126708903,
        "PHP": 48.072690897,
        "DKK": 6.1993164388,
        "HUF": 296.423807936,
        "CZK": 21.5863621207,
        "GBP": 0.7310186729,
        "RON": 4.0642714238,
        "SEK": 8.4493164388,
        "IDR": 14037.054018006,
        "INR": 72.8863787929,
        "BRL": 5.3588696232,
        "RUB": 75.5411803935,
        "HRK": 6.3116872291,
        "JPY": 105.2350783595,
        "THB": 30.0600200067,
        "CHF": 0.9018006002,
        "EUR": 0.8336112037,
        "MYR": 4.0585195065,
        "BGN": 1.6303767923,
        "TRY": 7.1265421807,
        "CNY": 6.463987996,
        "NOK": 8.6178726242,
        "NZD": 1.3892964321,
        "ZAR": 15.02975992,
        "USD": 1.0,
        "MXN": 20.2487495832,
        "SGD": 1.335861954,
        "AUD": 1.3110203401,
        "ILS": 3.2968489496,
        "KRW": 1118.089363121,
        "PLN": 3.7463321107
    },
    "base": "USD",
    "date": "2021-02-12"
  }*/

  /*let oExchangeRate = undefined;

  FuelCalculator.Storage.saveData("ExchangeRates", JSON.stringify(oExchangeRate));

  return oExchangeRate;*/
}

function requestRates() {

  fetch('https://api.exchangeratesapi.io/latest?base=USD')
  .then(response => response.json())
  .then(data => console.log(data));

  //fetch('https://api.exchangeratesapi.io/latest?base=USD').then((resp) => console.log());
  //.then((resp) => resp.json());
  //.then((data) => fx.rates = data.rates)
  //.then(demo);

  /*return Promise.resolve(
      $.ajax({
        contentType: 'application/json',
        url: 'https://api.exchangeratesapi.io/latest?base=USD',

        complete: function(data) {

          return data;
        }

      })
    );*/
}
