export function renderAnalysisTab() {

  let oData = {};
  FuelCalculator.Utils.executeHandlebars("analysisTabHolder", "AnalysisTab", oData, "clear");

  FuelCalculator.Analysis.renderTransportAnalysisTable();
}

export function onAnalysisTabPress() {

  renderAnalysisTab();
}
