export function renderTransportAnalysisTable() {

  let oData = {};

  let oTransportationSystems = getTransportationSystems();
  //let oCombustionTypes = FuelCalculator.Combustion.getCombustionTypes();
  let oFuelDefinitions = FuelCalculator.Fuels.getFuelDefinitions();

  var aKeys = Object.keys(oTransportationSystems);

  for (var i = 0; i < aKeys.length; i++) {

    let oFuel = FuelCalculator.Fuels.getFuelDefinition(oTransportationSystems[aKeys[i]].fuelId);
    if (oFuel) {
      oTransportationSystems[aKeys[i]].fuel = oFuel;
    } else {
      oTransportationSystems[aKeys[i]].fuel = {};
      oTransportationSystems[aKeys[i]].fuel.error = true;
    }

    /*let oCombustion = FuelCalculator.Combustion.getCombustionType(oTransportationSystems[aKeys[i]].combustionId);
    if (oCombustion) {
      oTransportationSystems[aKeys[i]].combustion = oCombustion;
    } else {
      oTransportationSystems[aKeys[i]].combustion = {};
      oTransportationSystems[aKeys[i]].combustion.error = true;
    }*/

    oTransportationSystems[aKeys[i]] = calculateTransportationSystems(oTransportationSystems[aKeys[i]]);

  }

  oData.systems = oTransportationSystems;

  oData.units = {};

  let oSystemUnits = FuelCalculator.Units.getTransportationSystemUnits();
  oData.units.system = oSystemUnits;

  let oTableUnits = FuelCalculator.Units.getTransportationAnalysisUnits();
  let oSelectedTableUnits = getTransportationAnalysisUnits();
  oData.units.analysis = oTableUnits;
  oData.units.selected = oSelectedTableUnits;

  //console.log(oData);

  FuelCalculator.Utils.executeHandlebars("transportationAnalysisTableHolder", "TransportationAnalysisTable", oData, "clear");
}

/*
 * TRANSPORTATION ANALYSIS SYSTEMS
 */
function getTransportationSystems() {

  let sTransportationSystems = FuelCalculator.Storage.getData("TransportationSystems");

  try {
    let oTransportationSystems = JSON.parse(sTransportationSystems);

    if (oTransportationSystems.systems) return oTransportationSystems.systems;

  } catch (e) {

    return initalizeTransportationSystems().systems;
  }
}

export function initalizeTransportationSystems() {

  let oTransportSystemsToSave = {}

  return saveTransportationSystems(oTransportSystemsToSave);
}

function saveTransportationSystems(oTransportSystemsToSave) {

  let iIterator = getTransportationIterator();
  let oTransportationSystems = {};
  oTransportationSystems.systems = oTransportSystemsToSave;
  oTransportationSystems.iterator = iIterator;

  FuelCalculator.Storage.saveData("TransportationSystems", JSON.stringify(oTransportationSystems));

  return oTransportationSystems;
}

function saveTransportationSystem(oTransportSystem) {

  let oTransportSystems = getTransportationSystems();
  oTransportSystems[oTransportSystem.id] = oTransportSystem;

  saveTransportationSystems(oTransportSystems);
}

function getTransportationIterator() {

  let sTransportationSystems = FuelCalculator.Storage.getData("TransportationSystems");

  let iIterators = 0;

  try {

    let oTransportationSystems = JSON.parse(sTransportationSystems);
    if (oTransportationSystems.iterator != null && oTransportationSystems.iterator != undefined) {

      return oTransportationSystems.iterator;

    }
    return 0;

  } catch (e) {
    return 0;
  }
}

function saveTransportationIterator(iIterator) {

  let oTransportationSystems = {};

  oTransportationSystems.systems = getTransportationSystems();
  oTransportationSystems.iterator = iIterator;

  FuelCalculator.Storage.saveData("TransportationSystems", JSON.stringify(oTransportationSystems));
}

function generateSystemId() {

  let iIterator = getTransportationIterator();

  saveTransportationIterator(iIterator += 1);

  return iIterator;
}

function getTransportationSystem(iSystemId) {

  return getTransportationSystems()[iSystemId];
}

/*
 * TRANSPORTATION ANALYSIS UNITS
 */
function getTransportationAnalysisUnits() {

  let sTransportationAnalysisUnits = FuelCalculator.Storage.getData("TransportationAnalysisUnits");

  try {
    let oTransportationAnalysisUnits = JSON.parse(sTransportationAnalysisUnits);

    if (oTransportationAnalysisUnits) return oTransportationAnalysisUnits;

  } catch (e) {

    return initalizeTransportationAnalysisUnits();
  }
}

export function initalizeTransportationAnalysisUnits() {

  let oTransportationUnitsToSave = FuelCalculator.Units.getDefaultTransportationAnalysisUnits();

  console.log("to save");
  console.log(oTransportationUnitsToSave);

  return saveTransportationAnalysisUnits(oTransportationUnitsToSave);
}

function saveTransportationAnalysisUnits(oTransportationUnitsToSave) {

  FuelCalculator.Storage.saveData("TransportationAnalysisUnits", JSON.stringify(oTransportationUnitsToSave));

  return oTransportationUnitsToSave;
}

export function onTransportationAnalysisUnitSelect(eElement, sType) {

  let sUnit = eElement.options[eElement.selectedIndex].value;

  let oTransportationUnits = getTransportationAnalysisUnits();
  oTransportationUnits[sType] = sUnit;

  saveTransportationAnalysisUnits(oTransportationUnits);
}

export function showAddEditTransportationSystemModal(iSystemId) {

  let oData = {};
  if (iSystemId != undefined && iSystemId != "") {
    oData = getTransportationSystem(iSystemId);
  }

  oData.view = {};

  let oUnits = FuelCalculator.Units.getTransportationSystemUnits();
  oData.view.units = oUnits;

  //let oCombustionTypes = FuelCalculator.Combustion.getCombustionTypes();
  //oData.view.combustionTypes = oCombustionTypes;

  let oFuelDefinitions = FuelCalculator.Fuels.getFuelDefinitions();
  oData.view.fuelDefinitions = oFuelDefinitions;

  if (iSystemId && iSystemId != "") {
    oData.view.edit = 1;
  } else {
    oData.view.edit = 0;
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "AddEditTransportSystemModal", oData, "clear");
}

export function onSaveTransportationSystem(iSystemId) {

  let bIsValid = true;

  let sSystemName = document.getElementById("transportationSystemNameField").value;

  let aNameFieldValidation = [{
    validation: "required"
  }];
  if (!FuelCalculator.Utils.validateField("transportationSystemNameField", "transportationSystemNameErrorHolder", sSystemName, aNameFieldValidation)) {
    bIsValid = false;
  }

  let sPrice = document.getElementById("priceField").value;
  let iPrice = parseFloat(sPrice, 10);
  let aPriceFieldValidation = [{
      validation: "required"
    },
    {
      validation: "number",
      gte: 0
    }
  ];
  if (!FuelCalculator.Utils.validateField("priceField", "priceErrorHolder", iPrice, aPriceFieldValidation)) bIsValid = false;

  let sEconomy = document.getElementById("economyField").value;
  let iEconomy = parseFloat(sEconomy, 10);
  let aEconomyFieldValidation = [{
      validation: "required"
    },
    {
      validation: "number",
      gte: 0
    }
  ];
  if (!FuelCalculator.Utils.validateField("economyField", "economyErrorHolder", iEconomy, aEconomyFieldValidation)) bIsValid = false;

  var ePriceUnitSelect = document.getElementById("priceUnitSelect");
  let sPriceUnit = ePriceUnitSelect.options[ePriceUnitSelect.selectedIndex].value;

  var eEconomyUnitSelect = document.getElementById("economyUnitSelect");
  let sEconomyUnit = eEconomyUnitSelect.options[eEconomyUnitSelect.selectedIndex].value;

  var eFuelSelect = document.getElementById("fuelSelect");
  let sFuelId = eFuelSelect.options[eFuelSelect.selectedIndex].value;

  //var eCombustionSelect = document.getElementById("combustionSelect");
  //let sCombusitonId = eCombustionSelect.options[eCombustionSelect.selectedIndex].value;

  /*let aCombustionIdSelectValidation = [{
      validation: "required"
    }
  ];
  if (!FuelCalculator.Utils.validateField("combustionSelect", "combustionErrorHolder", sCombusitonId, aCombustionIdSelectValidation)) bIsValid = false;
  */

  if (!bIsValid) return;

  let oTransportationSystem = {};

  oTransportationSystem.name = sSystemName;
  oTransportationSystem.fuelId = sFuelId;
  //oTransportationSystem.combustionId = sCombusitonId;
  oTransportationSystem.price = iPrice;
  oTransportationSystem.priceUnits = sPriceUnit;
  oTransportationSystem.fuelEconomy = iEconomy;
  oTransportationSystem.fuelEconomyUnits = sEconomyUnit;

  if (iSystemId == null || iSystemId == "") {
    // Generate new id
    iSystemId = generateSystemId();
  }

  oTransportationSystem.id = iSystemId;

  saveTransportationSystem(oTransportationSystem);

  FuelCalculator.Utils.closeModal();
  renderTransportAnalysisTable();
}

export function openDeleteTransportationConfirmationModel(iSystemId) {

  let oData = {
    header: "Delete System Definition",
    message: "Are you sure you want to delete the system definition?",
    callback: "FuelCalculator.Analysis.deleteTransportationSystem(" + iSystemId + ")"
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "ConfirmDeletionModal", oData, "clear");
}

export function deleteTransportationSystem(iSystemId) {

  let oTransportationSystems = getTransportationSystems();


  if (oTransportationSystems) {
    delete oTransportationSystems[iSystemId];

    saveTransportationSystems(oTransportationSystems);
  }

  FuelCalculator.Utils.closeModal();
  renderTransportAnalysisTable();

}

/*
 * Calculations
 */
function calculateTransportationSystems(oTransportationSystem) {

  //"onCalculate"

  //let oTransportationSystem = getTransportationSystem(oTransportationSystem.id);
  let oFuelDefinition = FuelCalculator.Fuels.getFuelDefinition(oTransportationSystem.fuelId);

  if(oFuelDefinition == null) {
    console.log("oFuelDefinition is null");
    return;
  }

  let iVolumetricEnergy = calculateVolumetricEnergy(oFuelDefinition);
  let iEnergyUsagePerMile = calculateEnergyUsagePerMile(iVolumetricEnergy, oTransportationSystem);
  let iPriceUSDPerMile =  calculatePricePerMile(oTransportationSystem);
  let iPriceUSDPerUnitEnergy = calculatePricePerUnitEnergy(iPriceUSDPerMile, iEnergyUsagePerMile);
  let iMassPerMile = calculateMassPerMile(oFuelDefinition, oTransportationSystem);
  let iCO2PerMile = calculateCO2MassPerMile(oFuelDefinition, iMassPerMile);
  let iCO2MassPerUnitEnergy = calculateCO2MassPerUnitEnergy(iCO2PerMile, iEnergyUsagePerMile);

  /*console.log("iVolumetricEnergy: " + iVolumetricEnergy);
  console.log("iEnergyUsagePerMile: " + iEnergyUsagePerMile);
  console.log("iPriceUSDPerMile: " + iPricePerMile);
  console.log("iPriceUSDPerUnitEnergy: " + iPricePerUnitEnergy);
  console.log("iMassPerMile: " + iMassPerMile);
  console.log("iCO2PerMile: " + iCO2PerMile);
  console.log("iCO2MassPerUnitEnergy: " + iCO2MassPerUnitEnergy);*/

  // TODO: DO UNIT CONVERSIONS
  //let sExchangeRate = FuelCalculator.ExchangeRates.getExchangeRate("EUR");
  //console.log(parseFloat(sExchangeRate, 10));

  oTransportationSystem.energyUsagePerMile = iEnergyUsagePerMile;
  oTransportationSystem.pricePerDistance = iPriceUSDPerMile;
  oTransportationSystem.pricePerEnergyUnit = iPriceUSDPerUnitEnergy;
  oTransportationSystem.co2PerDistance = iCO2PerMile;
  oTransportationSystem.co2PerUnitEnergy = iCO2MassPerUnitEnergy;

  return oTransportationSystem;
}

function calculateVolumetricEnergy(oFuelDefinition) {
  let iVolumetricEnergy = ((oFuelDefinition.lowerHeatingValue / 1000) * oFuelDefinition.fuelDensity * (1/264.172));
  return iVolumetricEnergy;
}

function calculateEnergyUsagePerMile(iVolumetricEnergy, oTransportationSystem) {
  return (iVolumetricEnergy/oTransportationSystem.fuelEconomy);
}

function calculatePricePerMile(oTransportationSystem) {
  return (oTransportationSystem.price / oTransportationSystem.fuelEconomy);
}

function calculatePricePerUnitEnergy(iPricePerMile, iEnergyUsagePerMile) {
  return (iPricePerMile / iEnergyUsagePerMile);
}

function calculateMassPerMile(oFuelDefinition, oTransportationSystem) {
  return (oFuelDefinition.fuelDensity * (1/264.172) * (1/oTransportationSystem.fuelEconomy));
}

function calculateCO2MassPerMile(oFuelDefinition, iMassPerMile) {
  return ((oFuelDefinition.equivalentMolecule.c / oFuelDefinition.molecularWeight) * 44.01 * iMassPerMile);
}

function calculateCO2MassPerUnitEnergy(iMassCO2PerMile, iEnergyUsagePerMile) {
  return (iMassCO2PerMile / iEnergyUsagePerMile);
}
