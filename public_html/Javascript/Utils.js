/**
 * Function to execute and append a precompiled handlebars template to a div.
 * To compile templates (unix terminal) from public_html/core/: sudo handlebars -m [templates folder] -f [path/name of compiled file]
 * sudo handlebars -m Templates/ Templates/Analysis Templates/Combustion Templates/Fuels -f Templates/templates.js
 *
 * @return boolean true on success.
 */
export function executeHandlebars(templateHolderSelector, templateName, jsonData, appendClearFlag, fCallback) {

  if (appendClearFlag === "clear") {
    $("#" + templateHolderSelector).html("");
  }

  $("#" + templateHolderSelector).append(Handlebars.templates[templateName](jsonData));

  if (fCallback && typeof(fCallback) === "function") {

    fCallback();
  }

}

export function openModal(callback, parameterList) {

  $('#modal').modal('show');

  if (callback && typeof(callback) === "function") {
    callback.apply(this, parameterList);
  }
}

export function closeModal() {

  $('#modal').modal('hide');
}

export function validateField(sFieldName, sFieldErrorHolder, sValue, aValidations) {

  FuelCalculator.Utils.removeFieldError(sFieldName, sFieldErrorHolder);

  let bIsValid = true;

  for (var i = 0; i < aValidations.length; i++) {
    var oValidation = aValidations[i];

    if (!oValidation.validation) return;

    if (oValidation.validation === "required") {
      if (sValue == "" || sValue == null) {
        FuelCalculator.Utils.addFieldError(sFieldName, sFieldErrorHolder, "Value is required.");
        bIsValid = false;
        break;
      }
    }
    if (oValidation.validation === "number") {

      let fValue = parseFloat(sValue);

      if (!Number.isFinite(fValue)) {
        FuelCalculator.Utils.addFieldError(sFieldName, sFieldErrorHolder, "Value must be a number.");
        bIsValid = false;
        break;
      }
      if (Number.isFinite(oValidation.gt)) {
        if (!(fValue > oValidation.gt)) {
          FuelCalculator.Utils.addFieldError(sFieldName, sFieldErrorHolder, "Value must be greater than " + oValidation.gt);
          bIsValid = false;
          break;
        }
      }
      if (Number.isFinite(oValidation.gte)) {
        if (!(fValue >= oValidation.gte)) {
          FuelCalculator.Utils.addFieldError(sFieldName, sFieldErrorHolder, "Value must be greater than or equal to " + oValidation.gte);
          bIsValid = false;
          break;
        }
      }
      if (Number.isFinite(oValidation.lt)) {
        if (!(fValue < oValidation.lt)) {
          FuelCalculator.Utils.addFieldError(sFieldName, sFieldErrorHolder, "Value must be less than " + oValidation.lt);
          bIsValid = false;
          break;
        }
      }
      if (Number.isFinite(oValidation.lte)) {
        if (!(fValue <= oValidation.lte)) {
          FuelCalculator.Utils.addFieldError(sFieldName, sFieldErrorHolder, "Value must be less than or equal to " + oValidation.lte);
          bIsValid = false;
          break;
        }
      }
    }
  }

  return bIsValid;
}

export function addFieldError(sFieldId, sFieldErrorHolderId, sErrorMessage) {
  this.addInputFieldErrorStyling(sFieldId);
  this.setFieldError(sFieldErrorHolderId, sErrorMessage);
  let oField = document.getElementById(sFieldId);
  oField.focus();
}

export function removeFieldError(sFieldId, sFieldErrorHolderId) {
  this.clearInputFieldStyling(sFieldId);
  this.setFieldError(sFieldErrorHolderId, "");
}

export function setFieldError(errorHolderId, message) {
  $("#" + errorHolderId).html(message);
}

export function addInputFieldErrorStyling(elementId) {
  // Change input focus class.
  this.setElementClass(elementId, "form-control is-invalid");
}

export function clearInputFieldStyling(elementId) {
  // Clear styling on field.
  this.setElementClass(elementId, "form-control");
}

export function setElementClass(elementId, classToSet) {

  document.getElementById(elementId).className = classToSet;
}

export function addElementClass(elementId, classToAdd) {
  document.getElementById(elementId).classList.add(classToAdd);
}

export function removeElementClass(elementId, classToRemove) {
  document.getElementById(elementId).classList.remove(classToRemove);
}
