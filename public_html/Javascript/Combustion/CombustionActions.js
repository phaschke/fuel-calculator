export function getCombustionTypes() {

  let sCombustionTypes = FuelCalculator.Storage.getData("CombustionTypes");

  try {
    let oParsedCombustionTypes = JSON.parse(sCombustionTypes);
    if (oParsedCombustionTypes.types) return oParsedCombustionTypes.types;

  } catch (e) {

    return null;
  }
}

export function getCombustionType(iCombustionId) {

  let oCombustionTypes = getCombustionTypes();
  if(!oCombustionTypes) return null;
  return oCombustionTypes[iCombustionId];
}

export function saveCombustionTypes(oCombustionTypesToSave) {

  let iIterator = getCombustionTypeIterator();
  let oCombustionTypes = {};
  oCombustionTypes.types = oCombustionTypesToSave;
  oCombustionTypes.iterator = iIterator;

  FuelCalculator.Storage.saveData("CombustionTypes", JSON.stringify(oCombustionTypes));
}

export function generateCombustionTypeId() {

  let iIterator = getCombustionTypeIterator();
  iterateCombustionTypeIterator();

  return iIterator;
}

export function getCombustionTypeIterator() {

  let sCombustionTypes = FuelCalculator.Storage.getData("CombustionTypes");

  try {

    let oParsedCombustionTypes = JSON.parse(sCombustionTypes);
    if (oParsedCombustionTypes.iterator != null && oParsedCombustionTypes.iterator != undefined) {

      return oParsedCombustionTypes.iterator;

    } else {
      return 0;
    }

  } catch (e) {
    return 0;
  }
}

export function iterateCombustionTypeIterator() {

  let oCombustionTypesToSave = getCombustionTypes()
  let iIterator = getCombustionTypeIterator() + 1;

  let oCombustionTypes = {};
  oCombustionTypes.types = oCombustionTypesToSave;
  oCombustionTypes.iterator = iIterator;

  FuelCalculator.Storage.saveData("CombustionTypes", JSON.stringify(oCombustionTypes));
}

export function renderCombustionTab() {

  let oCombustionTypes = getCombustionTypes();

  let oData = {}

  if (oCombustionTypes && (Object.keys(oCombustionTypes).length > 0)) {
    oData.error = false;
    oData.combustions = oCombustionTypes;

  } else {
    oData.error = true;
    oData.errorMsg = "No Combustion Types Defined.";
  }

  FuelCalculator.Utils.executeHandlebars("combustionTabHolder", "CombustionTab", oData, "clear");
}


export function showAddEditCombustionDefinitionModal(iCombustionId) {

  let oData = {
    view: {}
  };

  if (iCombustionId && iCombustionId != "") {
    oData.view.edit = 1;
    let oCombustionTypes = getCombustionTypes();

    if (!oCombustionTypes) {
      oCombustionTypes = {};
    }

    let oCombustion = oCombustionTypes[iCombustionId];
    if (!oCombustion) {
      // TODO: Display error modal
      return;
    }
    oData.combustion = oCombustion;

  } else {
    oData.view.edit = 0;
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "AddEditCombustionModal", oData, "clear");
}

export function saveCombustionDefinition(iEditingTypeId) {

  let oCombustionTypes = getCombustionTypes();

  if (oCombustionTypes == null) {
    oCombustionTypes = {};
  }

  // Validate data
  let sName = document.getElementById("combustionNameField").value;

  let bIsValid = true;
  let bIsRename = false;

  let aNameFieldValidation = [{
    validation: "required"
  }];

  if (!FuelCalculator.Utils.validateField("combustionNameField", "combustionNameErrorHolder", sName, aNameFieldValidation)) {
    bIsValid = false;

  } else {

    // Check if name already exists
    var aKeys = Object.keys(oCombustionTypes);

    for (var i = 0; i < aKeys.length; i++) {

      let oValue = oCombustionTypes[aKeys[i]];

      if ((oValue.name == sName) && (iEditingTypeId != oValue.key)) {
        // User attempting to add a second definition with same name
        FuelCalculator.Utils.addFieldError("combustionNameField", "combustionNameErrorHolder", "Combustion type with this name already exists.");
        bIsValid = false;
      }
    }
  }

  let iRatio = document.getElementById("equivalenceRatioField").value;

  let aRatioFieldValidation = [{
      validation: "required"
    },
    {
      validation: "number",
      gte: 0
    }
  ];

  if (!FuelCalculator.Utils.validateField("equivalenceRatioField", "equivalenceRatioErrorHolder", iRatio, aRatioFieldValidation)) bIsValid = false;

  var eOxidizerSelect = document.getElementById("oxidizerSelect");
  let sOxidizer = eOxidizerSelect.options[eOxidizerSelect.selectedIndex].value;

  if (!bIsValid) return;

  let oCombustionType = {
    name: sName,
    ratio: iRatio,
    oxidizer: sOxidizer
  }

  if (iEditingTypeId == null || iEditingTypeId == "") {
    // Generate new id
    iEditingTypeId = generateCombustionTypeId();
  }

  oCombustionType.id = iEditingTypeId;

  // Save combustion type
  oCombustionTypes[iEditingTypeId] = oCombustionType;


  saveCombustionTypes(oCombustionTypes);
  //FuelCalculator.Storage.saveData("CombustionTypes", JSON.stringify(oCombustionTypes));

  FuelCalculator.Utils.closeModal();
  renderCombustionTab();
}

export function openDeleteConfirmationModel(sCombustionName) {

  let oData = {
    header: "Delete Combustion Type Definition",
    message: "Are you sure you want to delete the combustion type definition?",
    callback: "FuelCalculator.Combustion.deleteCombustionType('" + sCombustionName + "')"
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "ConfirmDeletionModal", oData, "clear");
}

export function deleteCombustionType(iCombustionId) {

  let oCombustionTypes = getCombustionTypes();
  if (oCombustionTypes) {
    delete oCombustionTypes[iCombustionId];

    saveCombustionTypes(oCombustionTypes);
  }

  FuelCalculator.Utils.closeModal();
  renderCombustionTab();

}

export function openAllDeleteConfirmationModel() {

  let oData = {
    header: "Delete All Combustion Type Definitions",
    message: "Are you sure you want to delete ALL combustion type definitions?",
    callback: "FuelCalculator.Combustion.deleteAllCombustionTypes()"
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "ConfirmDeletionModal", oData, "clear");
}

export function deleteAllCombustionTypes() {

  FuelCalculator.Storage.saveData("CombustionTypes", JSON.stringify({}));
  FuelCalculator.Utils.closeModal();
  renderCombustionTab();

}
