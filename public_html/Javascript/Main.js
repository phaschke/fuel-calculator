// Utils
import * as utils from './Utils.js';
FuelCalculator.Utils = utils;

// Units
import * as unitDefinitions from './UnitDefinitions.js';
FuelCalculator.Units = unitDefinitions;

// ExchangeRate
import * as exchangeRates from './ExchangeRates.js';
FuelCalculator.ExchangeRates = exchangeRates;

// Storage Scripts
FuelCalculator.Storage = {};
import * as storage from './Storage.js';
FuelCalculator.Storage = Object.assign({}, storage);

// Dashboard Scripts
FuelCalculator.Dashboard = {};
import * as dashboard from './Dashboard/Dashboard.js';
FuelCalculator.Dashboard = Object.assign({}, dashboard);

// Analysis Scripts
FuelCalculator.Analysis = {};
import * as analysisActions from './Analysis/AnalysisActions.js';
import * as transportationAnalysis from './Analysis/TransportationAnalysis.js';
FuelCalculator.Analysis = Object.assign({}, analysisActions, transportationAnalysis);

// Fuel Scripts
FuelCalculator.Fuels = {};
import * as fuelActions from './Fuels/FuelActions.js';
import * as bondEnergyTableActions from './Fuels/BondEnergyTableActions.js'
import * as moleFractionTableActions from './Fuels/MoleFractionTableActions.js';
import * as fuelDetailsFormActions from './Fuels/FuelDetailsFormActions.js';
FuelCalculator.Fuels = Object.assign({}, fuelActions, bondEnergyTableActions, moleFractionTableActions, fuelDetailsFormActions);
import * as fuelDefinitions from './Fuels/FuelDefinitions.js';
FuelCalculator.Fuels.Definitions = fuelDefinitions;


// Compustion Scripts
/*FuelCalculator.Combustion = {};

import * as combustionActions from './Combustion/CombustionActions.js';
FuelCalculator.Combustion = Object.assign({}, combustionActions);*/

/**
 * Register Handlebars helpers
 */
Handlebars.registerHelper('select', function(value, options) {
  return options.fn(this)
    .split('\n')
    .map(function(v) {
      var t = 'value="' + value + '"'
      return !RegExp(t).test(v) ? v : v.replace(t, t + ' selected="selected"')
    })
    .join('\n')
});

Handlebars.registerHelper('round', function(fValue, iDecimalPlaces) {
  if(fValue === 0) return 0;
  if(isNaN(fValue) || fValue == "" || fValue == undefined) return "";
  return fValue.toFixed(iDecimalPlaces);
});

Handlebars.registerHelper('getDisplayUnitFromKey', function(sKey, aUnits) {
  for(var i = 0; i < aUnits.length; i++) {

    if(aUnits[i].key == sKey) {
      if(!aUnits[i].display) return sKey;
      return aUnits[i].display;
    }
  }

  return sKey;
});

Handlebars.registerHelper('size', function(obj) {

	if( typeof obj != "object" ) return;
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	if(size > 0) return 1;
  return 0;
});

Handlebars.registerHelper('convertBondEnergy', function(iBondEnergy) {
  if(iBondEnergy == undefined || iBondEnergy == 0 || iBondEnergy == "") return iBondEnergy;
  iBondEnergy = (iBondEnergy / 1000);
  return iBondEnergy.toFixed(2);
});

Handlebars.registerHelper('print', function(obj) {
  console.log(obj);
  return "";
});

/**
 * Register templates as partials
 */

Handlebars.registerPartial('FUEL_DEFINITION_VIEW', Handlebars.templates.FuelDefinitionView);

Handlebars.registerPartial('FUEL_DEFINITION_NAV', Handlebars.templates.FuelDefinitionNav);
Handlebars.registerPartial('FUEL_DEFINITION_PILL', Handlebars.templates.FuelDefinitionPill);

Handlebars.registerPartial('FUEL_DEFINITION_FORM', Handlebars.templates.FuelDefinitionForm);
Handlebars.registerPartial('FUEL_DEFINITION_FORM_BUTTONS', Handlebars.templates.FuelDefinitionEditButtons);
Handlebars.registerPartial('MOLE_FRACTION_TABLE', Handlebars.templates.MoleFractionTable);
Handlebars.registerPartial('BOND_ENERGY_TABLE', Handlebars.templates.BondEnergyTable);
Handlebars.registerPartial('FUEL_COMPARISON_TABLE', Handlebars.templates.FuelComparisonTable);

Handlebars.registerPartial('TRANSPORTATION_ANALYSIS', Handlebars.templates.TransportationAnalysis);
Handlebars.registerPartial('TRANSPORTATION_ANALYSIS_TABLE', Handlebars.templates.TransportationAnalysisTable);
