
export function openResetAllDataConfirmationModel() {

  let oData = {
    header: "Reset All Data",
    message: "Are you sure you want reset ALL data?",
    callback: "FuelCalculator.Dashboard.resetAllData()"
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "ConfirmDeletionModal", oData, "clear");
}

export function resetAllData() {

  FuelCalculator.Analysis.initalizeTransportationAnalysisUnits();
  FuelCalculator.Analysis.initalizeTransportationSystems();
  FuelCalculator.Analysis.renderTransportAnalysisTable();

  FuelCalculator.Fuels.initalizeDefaultFuels();
  FuelCalculator.Fuels.showFuelsTab();

  //FuelCalculator.Combustion.deleteAllCombustionTypes();
}
