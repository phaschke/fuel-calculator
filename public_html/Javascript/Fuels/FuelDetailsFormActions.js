/**
 * EDITING FUEL DETAILS
 *
 */
export function onEditFuelDetails(oFuelDefinition) {

  FuelCalculator.Utils.executeHandlebars("detailsTableHolder-" + oFuelDefinition.id, "FuelDefinitionFormEdit", oFuelDefinition, "clear");
}

export function onSaveFuelDetails(oFuelDefinition) {

  let bIsValid = true;

  let sFuelName = document.getElementById("fuelName-" + oFuelDefinition.id).value;

  let aNameFieldValidation = [{
    validation: "required"
  }];
  if (!FuelCalculator.Utils.validateField("fuelName-" + oFuelDefinition.id, "fuelNameErrorHolder-" + oFuelDefinition.id, sFuelName, aNameFieldValidation)) {

    bIsValid = false;

  } else {

    // If the fuel name has been changed, check for name unique
    if (sFuelName != oFuelDefinition.name) {
      if (!validateUniqueFuelName(sFuelName)) {
        FuelCalculator.Utils.addFieldError("fuelName-" + oFuelDefinition.id, "fuelNameErrorHolder-" + oFuelDefinition.id, "Fuel type with this name already exists.");
        bIsValid = false;
      }
    }

  }

  let sDensity = document.getElementById("fuelDensity-" + oFuelDefinition.id).value;
  let aDensityFieldValidation = [{
      validation: "required"
    },
    {
      validation: "number",
      gte: 0
    }
  ];

  if (!FuelCalculator.Utils.validateField("fuelDensity-" + oFuelDefinition.id, "fuelDensityErrorHolder-" + oFuelDefinition.id, sDensity, aDensityFieldValidation)) bIsValid = false;
  let iDensity = parseFloat(sDensity);

  let eDensityUnitSelect = document.getElementById("fuelDensityUnit-" + oFuelDefinition.id);
  let sDensityUnit = eDensityUnitSelect.options[eDensityUnitSelect.selectedIndex].value;

  let bUseDefinedHeatingValue = false;
  if(document.getElementById("heatingValueCheckbox-" + oFuelDefinition.id).checked == true) {
    bUseDefinedHeatingValue = true;
  }

  let sDefinedHeatingValue = document.getElementById("fuelHeatingValue-" + oFuelDefinition.id).value;
  if(bUseDefinedHeatingValue) {

    let aDefinedHeatingValueValidation = [{
        validation: "required"
      },
      {
        validation: "number",
      }
    ];
    if (!FuelCalculator.Utils.validateField("fuelHeatingValue-" + oFuelDefinition.id, "fuelHeatingValueErrorHolder-" + oFuelDefinition.id, sDefinedHeatingValue, aDefinedHeatingValueValidation)) bIsValid = false;
  }

  if (!bIsValid) return false;

  oFuelDefinition.name = sFuelName;
  oFuelDefinition.fuelDensity = iDensity;
  oFuelDefinition.fuelDensityUnit = sDensityUnit;

  if(bUseDefinedHeatingValue) {
    oFuelDefinition.useDefinedHeatingValue = true;
    let iDefinedHeatingValue = parseFloat(sDefinedHeatingValue, 10);
    oFuelDefinition.lowerHeatingValue = iDefinedHeatingValue;
  } else {
    oFuelDefinition.useDefinedHeatingValue = false;
  }

  FuelCalculator.Fuels.saveFuelDefinition(oFuelDefinition);

  return true;
}

function validateUniqueFuelName(sFuelName) {

  let oFuelDefinitions = FuelCalculator.Fuels.getFuelDefinitions();

  // Check if name already exists
  var aKeys = Object.keys(oFuelDefinitions);

  for (var i = 0; i < aKeys.length; i++) {

    let oValue = oFuelDefinitions[aKeys[i]];

    if ((oValue.name == sFuelName)) {
      // User attempting to add a second definition with same name
      return false;
    }
  }
  return true;
}

export function onUseDefinedHeatingValueCheck(eElement) {

  let sElementId = eElement.id;
  let iFuelId = sElementId.split("-")[1];

  console.log("check");

  if(document.getElementById("heatingValueCheckbox-" + iFuelId).checked == true) {

    document.getElementById("fuelHeatingValue-" + iFuelId).readOnly = false;
  } else {

    document.getElementById("fuelHeatingValue-" + iFuelId).readOnly = true;
  }

}
