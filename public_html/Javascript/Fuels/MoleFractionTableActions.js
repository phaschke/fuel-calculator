/**
 * EDITING MOLE FRACTION TABLE
 *
 */
export function onEditMoleFractionTable(oFuelDefinition) {

  FuelCalculator.Utils.executeHandlebars("moleFractionTableHolder-" + oFuelDefinition.id, "MoleFractionTableEdit", oFuelDefinition, "clear");
}

export function onSaveMoleFractionTable(oFuelDefinition) {

  let oAtomicStructure = getAtomicStructure(oFuelDefinition.id);

  let oResult = onMoleFractionValidation(oFuelDefinition.id);
  if (oResult.isError) return false;

  oFuelDefinition.MoleFraction = oResult.MoleFractionValues;
  oFuelDefinition.AtomicStructure = oAtomicStructure;
  oFuelDefinition.AtomicStructure.molecularWeight = getMolecularWeights(oFuelDefinition.id, oAtomicStructure);

  FuelCalculator.Fuels.saveFuelDefinition(oFuelDefinition);

  return true;
}

export function getMoleFractionValues(iFuelId) {

  let oFuelDefinition = FuelCalculator.Fuels.getFuelDefinition(iFuelId);
  return oFuelDefinition.MoleFraction;
}


export function onMoleFractionInput(eElement) {

  let iMoleFractionSum = 0;

  let sFieldId = eElement.id;
  let sJSONKey = "field" + sFieldId.split("-")[1];
  let sFuelId = sFieldId.split("-")[2];

  let fValue = parseFloat(eElement.value, 10);
  if (isNaN(fValue) || fValue < 0 || fValue > 1) {
    fValue = 0;
    eElement.value = fValue;
  }
  let oResult = onMoleFractionValidation(sFuelId);

  document.getElementById("MoleFraction-sum-" + sFuelId).innerHTML = oResult.MoleFractionValues.sum;

  if(!oResult.isError) {
    FuelCalculator.Fuels.refreshBondEnergy(sFuelId);
  }

}

export function getMoleFractionValuesFromTable(iFuelId) {

  let iMoleFractionSum = 0;

  let oMoleFractionTable = {};
  for(var i = 1; i < 7; i++) {

    var sValue = document.getElementById("MoleFraction-" + i + "-" + iFuelId).value;
    let iValue = parseFloat(sValue, 10);

    oMoleFractionTable["field" + i] = iValue;
    iMoleFractionSum += iValue;
  }

  oMoleFractionTable.sum = iMoleFractionSum.toFixed(1);

  return oMoleFractionTable;
}

export function onMoleFractionValidation(sFuelId) {

  let oResult = {
    isError: false,
    errorFields: [],
    MoleFractionValues: {}
  }

  let iMoleFractionSum = 0;

  let oMoleFractionTable = {};
  for(var i = 1; i < 7; i++) {

    var sValue = document.getElementById("MoleFraction-" + i + "-" + sFuelId).value;
    // Default blank fields
    if (sValue == "" || sValue == undefined) sValue = 0;

    let iValue = parseFloat(sValue, 10);

    oMoleFractionTable["field" + i] = iValue;
    iMoleFractionSum += iValue;

    if (iValue != 0) {
      oResult.errorFields.push("MoleFraction-" + i + "-" + sFuelId);
    }
    // Clear all field errors
    FuelCalculator.Utils.clearInputFieldStyling("MoleFraction-" + i + "-" + sFuelId);

  }

  oResult.MoleFractionValues = oMoleFractionTable;
  oResult.MoleFractionValues.sum = iMoleFractionSum.toFixed(1);

  if (iMoleFractionSum != 1.0) {
    oResult.isError = true;
  }
  // If value is 0, highlight all fields
  if (iMoleFractionSum == 0) {
    oResult.errorFields = [];
    for (let i = 1; i < 7; i++) {
      oResult.errorFields.push("MoleFraction-" + i + "-" + sFuelId);
    }
  }

  if (oResult.isError) {
    for (var i = 0; i < oResult.errorFields.length; i++) {
      FuelCalculator.Utils.addInputFieldErrorStyling(oResult.errorFields[i]);
    }
    document.getElementById("moleFractionTableError-" + sFuelId).innerHTML = "The sum of the mole fraction must equal 1.";

  } else {
    document.getElementById("moleFractionTableError-" + sFuelId).innerHTML = "";
  }

  return oResult;

}

export function onAtomicStrutureChange(iFuelId) {

  let oAtomicStructure = getAtomicStructure(iFuelId);
  let oMolecularWeights = getMolecularWeights(iFuelId, oAtomicStructure);

  for(var i = 1; i < 7; i++) {
    document.getElementById("weight-bond" + i).innerHTML = oMolecularWeights["bond" + i].toFixed(1);
  }

  FuelCalculator.Fuels.refreshBondEnergy(iFuelId);
}

export function getAtomicStructure(iFuelId) {

  let oAtomicStructure = {};

  for (var i = 1; i < 7; i++) {

    var c = document.getElementById("C-" + i + "-" + iFuelId).value;
    var h = document.getElementById("H-" + i + "-" + iFuelId).value;
    var o = document.getElementById("O-" + i + "-" + iFuelId).value;
    var n = document.getElementById("N-" + i + "-" + iFuelId).value;

    oAtomicStructure["c" + i] = c;
    oAtomicStructure["h" + i] = h;
    oAtomicStructure["o" + i] = o;
    oAtomicStructure["n" + i] = n;
  }

  return oAtomicStructure;

}

export function getMolecularWeights(iFuelId, oAtomicStructure) {

  let oMolecularWeights = {};

  for (var i = 1; i < 7; i++) {

    let iMolecularWeight = 0;

    iMolecularWeight += (FuelCalculator.Fuels.getOrDefaultNumber(oAtomicStructure["c" + i]) * 12.0107);
    iMolecularWeight += (FuelCalculator.Fuels.getOrDefaultNumber(oAtomicStructure["h" + i]) * 1.0079);
    iMolecularWeight += (FuelCalculator.Fuels.getOrDefaultNumber(oAtomicStructure["o" + i]) * 15.9994);
    iMolecularWeight += (FuelCalculator.Fuels.getOrDefaultNumber(oAtomicStructure["n" + i]) * 14.0067);

    oMolecularWeights["bond" + i] = iMolecularWeight;
  }
  return oMolecularWeights;
}
