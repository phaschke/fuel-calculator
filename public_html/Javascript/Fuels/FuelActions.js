function getFuels() {
  let sFuels = FuelCalculator.Storage.getData("Fuels");

  try {
    let oParsedFuels = JSON.parse(sFuels);
    return oParsedFuels;

  } catch (e) {

    return null;
  }
}

export function getFuelDefinitions(callback) {

  let oParsedFuels = getFuels();

  if (oParsedFuels != null && oParsedFuels.definitions != undefined) {
    if (Object.keys(oParsedFuels.definitions).length >= 3) {

      if (callback && typeof(callback) === "function") {
        callback(oParsedFuels.definitions);
      }

      return oParsedFuels.definitions;
    }
  }

  return initalizeDefaultFuels().definitions;
}

export function getFuelDefinition(iFuelId) {
  return getFuelDefinitions()[iFuelId];
}

function saveFuels(oFuels) {

  FuelCalculator.Storage.saveData("Fuels", JSON.stringify(oFuels));
}

export function saveFuelDefinition(oFuelDefinition) {

  let oFuels = {}

  let oFuelDefinitions = getFuelDefinitions();
  oFuelDefinitions[oFuelDefinition.id] = oFuelDefinition;
  oFuels.definitions = oFuelDefinitions;

  oFuels.iterator = getFuelDefinitionIterator();

  saveFuels(oFuels);
}

export function getFuelDefinitionIterator() {

  let oParsedFuels = getFuels();
  if (oParsedFuels.iterator != null && oParsedFuels.iterator != undefined) {

    return oParsedFuels.iterator;

  } else {

    initalizeDefaultFuels();
  }
}

export function iterateFuelDefinitionTypeIterator() {

  let oFuelDefinitionTypesToSave = getFuelDefinitions()
  let iIterator = getFuelDefinitionIterator() + 1;

  let oFuels = {};
  oFuels.definitions = oFuelDefinitionTypesToSave;
  oFuels.iterator = iIterator;

  saveFuels(oFuels);
}

export function deleteFuelDefinitionType(iFuelId) {

  let oFuelDefinitions = getFuelDefinitions();
  if (oFuelDefinitions) {

    delete oFuelDefinitions[iFuelId];

    let oFuels = {}
    oFuels.definitions = oFuelDefinitions;
    oFuels.iterator = getFuelDefinitionIterator();

    saveFuels(oFuels);
  }
}

export function initalizeDefaultFuels() {

  let oFuels = {};

  let oDefinitions = {};
  let oGasolineDefinition = FuelCalculator.Fuels.Definitions.getDefaultGasolineDefinition();
  oGasolineDefinition = calculateValues(oGasolineDefinition);
  oDefinitions[oGasolineDefinition.id] = oGasolineDefinition;
  let oDieselDefinition = FuelCalculator.Fuels.Definitions.getDefaultDieselDefinition();
  oDieselDefinition = calculateValues(oDieselDefinition);
  oDefinitions[oDieselDefinition.id] = oDieselDefinition;
  let oEthanolDefinition = FuelCalculator.Fuels.Definitions.getDefaultEthanolDefinition();
  oEthanolDefinition = calculateValues(oEthanolDefinition);
  oDefinitions[oEthanolDefinition.id] = oEthanolDefinition;

  oFuels.definitions = oDefinitions;
  oFuels.iterator = 3;

  saveFuels(oFuels);

  return oFuels;
}

export function showFuelsTab(iActiveTabId) {

  let oData = {};

  let oFuelDefinitions = getFuelDefinitions();
  oData.fuels = oFuelDefinitions;

  FuelCalculator.Utils.executeHandlebars("fuelsTabHolder", "FuelsTab", oData, "clear");

  if (iActiveTabId != null && iActiveTabId != undefined) {
    setActiveTab(iActiveTabId);
  }
}

export function onOverviewTabClick() {

  let oData = {};

  let oFuelDefinitions = getFuelDefinitions();
  oData.fuels = oFuelDefinitions;

  console.log(oData.fuels);

  FuelCalculator.Utils.executeHandlebars("fuelOverviewTableHolder", "FuelComparisonTable", oData, "clear");
}

function setActiveTab(iActiveTabId) {

  FuelCalculator.Utils.removeElementClass("pills-overview-tab", "active");
  FuelCalculator.Utils.removeElementClass("pills-overview-tab", "show");
  FuelCalculator.Utils.removeElementClass("pills-overview", "active");
  FuelCalculator.Utils.removeElementClass("pills-overview", "show");

  FuelCalculator.Utils.addElementClass("pills-" + iActiveTabId + "-tab", "active");
  FuelCalculator.Utils.addElementClass("pills-" + iActiveTabId + "-tab", "show");
  FuelCalculator.Utils.addElementClass("pills-" + iActiveTabId, "active");
  FuelCalculator.Utils.addElementClass("pills-" + iActiveTabId, "show");

}

export function openResetFuelConfirmationModel(sFuelType) {

  let oData = {
    header: "Reset Fuel",
    message: "Are you sure you want to reset this fuel definition?",
    callback: "FuelCalculator.Fuels.resetFuelDefinition('" + sFuelType + "')"
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "ConfirmDeletionModal", oData, "clear");
}

export function resetFuelDefinition(sFuelType) {

  let oFuelDefinition = {};

  switch (sFuelType) {
    case "Gasoline":
      oFuelDefinition = FuelCalculator.Fuels.Definitions.getDefaultGasolineDefinition();
      break;

    case "Diesel":
      oFuelDefinition = FuelCalculator.Fuels.Definitions.getDefaultDieselDefinition();
      break;

    case "Ethanol":
      oFuelDefinition = FuelCalculator.Fuels.Definitions.getDefaultEthanolDefinition();
      break;

    default:
      return;
  }

  oFuelDefinition = calculateValues(oFuelDefinition);

  saveFuelDefinition(oFuelDefinition);
  showFuelsTab(oFuelDefinition.id);
}


/**
 * CREATION OF NEW FUEL
 *
 */

export function showNameNewFuelModal(sFuelType) {

  let oData = {};

  if (sFuelType) {
    oData.name = "CopyOf" + sFuelType;
    oData.type = sFuelType;
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "NewFuelNameModal", oData, "clear", function() {
    let oElement = document.getElementById("newFuelNameField");
    oElement.select();
  });

}

export function onNewFuelNameSelect(sFuelType) {

  let sNameField = "newFuelNameField";
  let sNameFieldErrorHolder = "newFuelNameErrorHolder";
  let eNameField = document.getElementById(sNameField);
  let sName = eNameField.value;

  let aNameFieldValidation = [{
    validation: "required"
  }];

  if (!FuelCalculator.Utils.validateField("newFuelNameField", "newFuelNameErrorHolder", sName, aNameFieldValidation)) {
    document.getElementById("newFuelNameField").select();
    return;
  }

  if (!validateNameUnique(sName, sNameField, sNameFieldErrorHolder)) {
    document.getElementById("newFuelNameField").select();
    return;
  }

  createNewFuel(sName, sFuelType);

}

export function createNewFuel(sFuelName, sFuelType) {

  let iFuelId = getFuelDefinitionIterator();
  iterateFuelDefinitionTypeIterator();

  let oNewFuelDefinition = FuelCalculator.Fuels.Definitions.getNewDefinition(iFuelId, sFuelName, sFuelType);

  oNewFuelDefinition = calculateValues(oNewFuelDefinition);

  saveFuelDefinition(oNewFuelDefinition);
  showFuelsTab(iFuelId);


  FuelCalculator.Utils.closeModal();

}

export function openDeleteConfirmationModel(iFuelId) {

  let oData = {
    header: "Delete Fuel",
    message: "Are you sure you want to delete this fuel definition?",
    callback: "FuelCalculator.Fuels.deleteFuelDefinition(" + iFuelId + ")"
  }

  FuelCalculator.Utils.executeHandlebars("modal-content", "ConfirmDeletionModal", oData, "clear");
}

export function deleteFuelDefinition(iFuelId) {

  deleteFuelDefinitionType(iFuelId);
  showFuelsTab();
  FuelCalculator.Utils.closeModal();

}

function validateNameUnique(sName, sNameField, sNameFieldErrorHolder) {

  let oDefinitions = getFuelDefinitions();

  let aKeys = Object.keys(oDefinitions);

  for (var i = 0; i < aKeys.length; i++) {

    let oValue = oDefinitions[aKeys[i]];
    if (oValue.name == sName) {
      // User attempting to add a second definition with same name
      FuelCalculator.Utils.addFieldError(sNameField, sNameFieldErrorHolder, "Fuel definition with this name already exists.");
      return false;
    }
  }
  return true;
}


/**
 * EDITING EXISITING FUEL
 *
 */
export function onEdit(iFuelId) {

  let oFuelDefinition = getFuelDefinition(iFuelId);

  onShowEditSaveCancelButtons(oFuelDefinition);
  FuelCalculator.Fuels.onEditFuelDetails(oFuelDefinition);
  FuelCalculator.Fuels.onEditMoleFractionTable(oFuelDefinition);
  FuelCalculator.Fuels.onEditBondEnergyTable(oFuelDefinition);
}

export function onCancel(iFuelId) {

  let oFuelDefinition = getFuelDefinition(iFuelId);

  showFuelsTab(iFuelId);
}

export function onSave(iFuelId) {

  let oFuelDefinition = getFuelDefinition(iFuelId);
  let bIsValid = true;

  if(!FuelCalculator.Fuels.onSaveFuelDetails(oFuelDefinition)) bIsValid = false;
  oFuelDefinition = getFuelDefinition(iFuelId);

  if(!FuelCalculator.Fuels.onSaveMoleFractionTable(oFuelDefinition)) bIsValid = false;
  oFuelDefinition = getFuelDefinition(iFuelId);

  if(!FuelCalculator.Fuels.onSaveBondEnergyTable(oFuelDefinition)) bIsValid = false;

  if (bIsValid) {
    // Do calculations for new fuel
    onCalculateValues(iFuelId);
    showFuelsTab(iFuelId);
  }
}

/**
 * EDITING FUEL BUTTONS
 *
 */
function onShowFuelEditButton(oFuelDefinition) {

  FuelCalculator.Utils.executeHandlebars("fuelFormButtonsHolder-" + oFuelDefinition.id, "FuelDefinitionEditButtons", oFuelDefinition, "clear");
}

function onShowEditSaveCancelButtons(oFuelDefinition) {

  FuelCalculator.Utils.executeHandlebars("fuelFormButtonsHolder-" + oFuelDefinition.id, "FuelDefinitionEditButtonsEdit", oFuelDefinition, "clear");
}


export function getBondEnergyConstant(sBond) {

  let oBondEnergyConstants = {
    c1c: 347000,
    c2c: 614000,
    c3c: 839000,
    c1h: 413000,
    c1n: 305000,
    c2n: 615000,
    c3n: 891000,
    c1o: 358000,
    c2o: 804000,
    c3o: 1072000,
    h1h: 432000,
    n1h: 391000,
    n1n: 160000,
    n2n: 418000,
    n3n: 941000,
    n1o: 201000,
    n2o: 607000,
    o1h: 467000,
    o1o: 146000,
    o2o: 495000
  }

  if (sBond == undefined || sBond == "") {
    return oBondEnergyConstants;
  }

  return oBondEnergyConstants[sBond];

}

/**
 * CALCULATE VALUES
 *
 */

export function onCalculateValues(iFuelId) {

  let oFuelDefinition = getFuelDefinition(iFuelId);
  oFuelDefinition = calculateValues(oFuelDefinition);

  saveFuelDefinition(oFuelDefinition);
}

function calculateValues(oFuelDefinition) {

  let oBondEnergy = oFuelDefinition.BondEnergy;

  let oEquivalentMolecule = calculateEquivalentMolecule(oFuelDefinition);
  let oBalances = calculateBalances(oEquivalentMolecule);
  let iStoichiometricAirMass = calculateStoichiometricAirMass(oBalances);
  let iMolecularWeight = calculateMolecularWeight(oEquivalentMolecule);
  let iStoichiometricAirFuelRatio = calculateStoichiometricAirFuelRatio(iStoichiometricAirMass, iMolecularWeight);

  // Do not calculate the heating value if use entered
  if(!oFuelDefinition.useDefinedHeatingValue) {
    let iLowerHeatingValue = calculateLowerHeatValue(oBalances, oBondEnergy, iMolecularWeight);
    oFuelDefinition.lowerHeatingValue = iLowerHeatingValue;
  }

  oFuelDefinition.equivalentMolecule = oEquivalentMolecule;
  oFuelDefinition.molecularWeight = iMolecularWeight;
  oFuelDefinition.stoichAirMass = iStoichiometricAirMass;
  oFuelDefinition.stoichAFR = iStoichiometricAirFuelRatio;

  return oFuelDefinition;
}

function calculateLowerHeatValue(oBalances, oBondEnergy, iMolecularWeight) {

  let iReactants = calculateReactants(oBalances, oBondEnergy);
  let iProducts = calculateProducts(oBalances);

  let iLowerHeatingValue = ((-iProducts.co2) + (-iProducts.h2o) + (-iProducts.n2)) - ((-iReactants.o2) + (-iReactants.n2) + (-iReactants.fuel));

  return Math.abs(iLowerHeatingValue / iMolecularWeight);
}

function calculateReactants(oBalances, oBondEnergy) {

  let iO2 = (oBalances.w * getBondEnergyConstant("o2o"));
  let iN2 = (3.76 * oBalances.w * getBondEnergyConstant("n3n"));
  let iTotalBondEnergy = oBondEnergy.bondEnergyPerMolecule.total;

  return {
    o2: iO2,
    n2: iN2,
    fuel: iTotalBondEnergy
  };
}

function calculateProducts(oBalances) {

  let iCO2 = (oBalances.x * 2 * getBondEnergyConstant("c2o"));
  let iH2O = (oBalances.y * 2 * getBondEnergyConstant("o1h"));
  let iN2 = (oBalances.z * getBondEnergyConstant("n3n"));

  return {
    co2: iCO2,
    h2o: iH2O,
    n2: iN2
  };

}

function calculateStoichiometricAirFuelRatio(iStoichiometricAirMass, iMolecularWeight) {
  return (iStoichiometricAirMass / iMolecularWeight);
}

export function calculateMolecularWeight(oEquivalentMolecule) {

  return ((12.0107 * oEquivalentMolecule.c) + (1.0079 * oEquivalentMolecule.h) + (15.9994 * oEquivalentMolecule.o) + (14.0067 * oEquivalentMolecule.n));
}

function calculateStoichiometricAirMass(oBalances) {

  return (137.3291 * oBalances.w);
}

function calculateBalances(oEquivalentMolecule) {

  let x = oEquivalentMolecule.c;
  let y = (oEquivalentMolecule.h / 2);
  let w = .5 * ((2 * x) + (y) - oEquivalentMolecule.o);
  let z = .5 * (oEquivalentMolecule.n + (7.52 * x) + (1.88 * oEquivalentMolecule.h) - (3.76 * oEquivalentMolecule.o));

  let oBalances = {
    x: x,
    y: y,
    w: w,
    z: z
  }

  return oBalances;
}

export function calculateEquivalentMolecule(oFuelDefinition) {

  let oMoleFractions = oFuelDefinition.MoleFraction;
  let oAtomicStructure = oFuelDefinition.AtomicStructure;

  let iCSum = 0;
  let iHSum = 0;
  let iOSum = 0;
  let iNSum = 0;

  for (var i = 1; i < 7; i++) {

    let iMoleFraction = oMoleFractions["field" + i];

    iCSum += (getOrDefaultNumber(oAtomicStructure["c" + i]) * iMoleFraction);
    iHSum += (getOrDefaultNumber(oAtomicStructure["h" + i]) * iMoleFraction);
    iOSum += (getOrDefaultNumber(oAtomicStructure["o" + i]) * iMoleFraction);
    iNSum += (getOrDefaultNumber(oAtomicStructure["n" + i]) * iMoleFraction);
  }

  return {
    c: iCSum,
    h: iHSum,
    o: iOSum,
    n: iNSum
  }
}

export function getOrDefaultNumber(iValue) {

  if (isNaN(iValue) || iValue == "") return 0;
  return parseFloat(iValue);
}
