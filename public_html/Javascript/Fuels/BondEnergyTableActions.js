/**
 *
 * BOND ENERGY TABLE
 *
 */
let _BOND_ENERGY_TABLE_BUFFER_ = {};

export function onEditBondEnergyTable(oFuelDefinition) {

  _BOND_ENERGY_TABLE_BUFFER_ = oFuelDefinition.BondEnergy;

  FuelCalculator.Utils.executeHandlebars("bondEnergyTableHolder-" + oFuelDefinition.id, "BondEnergyTableEdit", oFuelDefinition, "clear");
}

export function onSaveBondEnergyTable(oFuelDefinition) {

  let oBondTableTableBuffer = _BOND_ENERGY_TABLE_BUFFER_;

  oFuelDefinition.BondEnergy = oBondTableTableBuffer;

  onCalculateAndReturnBondTable(oFuelDefinition);

  FuelCalculator.Fuels.saveFuelDefinition(oFuelDefinition);

  return true;
}

export function onCalculateAndReturnBondTable(oFuelDefinition) {

  _BOND_ENERGY_TABLE_BUFFER_ = oFuelDefinition.BondEnergy;

  return calculateFormValues(_BOND_ENERGY_TABLE_BUFFER_, oFuelDefinition.id);

}

export function onBondEnergyInput(eElement) {

  let sFieldId = eElement.id;
  let aIdSplit = sFieldId.split("-");
  let sBondNumber = aIdSplit[0];
  let sBond = aIdSplit[1];
  let iFuelId = aIdSplit[2];

  let iValue = parseInt(eElement.value, 10);
  if (isNaN(iValue) || iValue < 0) {
    iValue = "";
    eElement.value = "";
  }

  _BOND_ENERGY_TABLE_BUFFER_.fields[sBondNumber][sBond] = iValue;

  refreshBondEnergy(iFuelId);
}

export function refreshBondEnergy(iFuelId) {

  _BOND_ENERGY_TABLE_BUFFER_ = calculateFormValues(_BOND_ENERGY_TABLE_BUFFER_, iFuelId);

  let oBondEnergyTableBuffer = {
    id: iFuelId,
    BondEnergy: _BOND_ENERGY_TABLE_BUFFER_
  }

  FuelCalculator.Utils.executeHandlebars("bondEnergyTableHolder-" + iFuelId, "BondEnergyTableEdit", oBondEnergyTableBuffer, "clear");
}


export function calculateFormValues(oBondEnergyTableBuffer, iFuelId) {

  let oMoleFractionValues = FuelCalculator.Fuels.getMoleFractionValuesFromTable(iFuelId);

  let oBondEnergyConstants = FuelCalculator.Fuels.getBondEnergyConstant();
  let iTotalBondEnergySum = 0;
  let oBondEnergiesByType = getBondEnergyByTypeJSON();

  for (var i = 1; i < 7; i++) {

    let iBondsPerMoleculeSum = 0;
    let iBondEnergySum = 0;

    let iMoleFractionValue = oMoleFractionValues["field" + i];

    for (var sBond in oBondEnergyConstants) {

      let iBondValue = getBondValue(i, sBond);

      iBondsPerMoleculeSum += iBondValue;
      let iBondEnergyValue = iBondValue * FuelCalculator.Fuels.getBondEnergyConstant(sBond);
      iBondEnergySum += iBondEnergyValue;

      oBondEnergiesByType[sBond] += (iBondEnergyValue * iMoleFractionValue);
    }

    oBondEnergyTableBuffer.bondTotalPerMolecule["bond" + i] = iBondsPerMoleculeSum;
    oBondEnergyTableBuffer.bondEnergyPerMolecule["bond" + i] = iBondEnergySum;

    // Sum ALL bond energies
    iTotalBondEnergySum += (iBondEnergySum * iMoleFractionValue);
  }

  oBondEnergyTableBuffer.energyFromBondType = oBondEnergiesByType;
  oBondEnergyTableBuffer.bondEnergyPerMolecule.total = iTotalBondEnergySum;

  // Calculate percent energy from Molecule
  for (var i = 0; i < 7; i++) {


    let iBondEnergy = oBondEnergyTableBuffer.bondEnergyPerMolecule["bond" + i];
    let iMoleFractionValue = oMoleFractionValues["field" + i];

    let iPercentEnergyPerMolecule = (((iBondEnergy * iMoleFractionValue) / iTotalBondEnergySum) * 100).toFixed(1);

    if (isNaN(iPercentEnergyPerMolecule)) iPercentEnergyPerMolecule = 0;

    oBondEnergyTableBuffer.percentEnergyPerMolecule["bond" + i] = iPercentEnergyPerMolecule;
  }

  // Calculate percent energy from Bond Type
  for (var sBond in oBondEnergiesByType) {

    let iBondEnergyFromType = oBondEnergiesByType[sBond];
    let iPercentEnergyPerType = ((iBondEnergyFromType / iTotalBondEnergySum) * 100).toFixed(1);

    if (isNaN(iPercentEnergyPerType)) iPercentEnergyPerType = 0;

    oBondEnergyTableBuffer.percentEnergyFromBondType[sBond] = iPercentEnergyPerType;
  }

  oBondEnergyTableBuffer.totalBondEnergy = getBondEnergyKg(iFuelId, oBondEnergyTableBuffer, oMoleFractionValues);

  return oBondEnergyTableBuffer;
}

function getBondValue(iBondNumber, sBond) {

  let iValue = _BOND_ENERGY_TABLE_BUFFER_.fields["bond" + iBondNumber][sBond];
  if (iValue == "" || iValue == "") return 0;
  return parseInt(iValue, 10);
}

export function onUpdateBondEnergies(iFuelId) {

  console.log(_BOND_ENERGY_TABLE_BUFFER_);
}

function getBondEnergyKg(iFuelId, oBondEnergyTableBuffer, oMoleFractionValues) {

  let oAtomicStructure = FuelCalculator.Fuels.getAtomicStructure(iFuelId);
  let oMolecularWeight = FuelCalculator.Fuels.getMolecularWeights(iFuelId, oAtomicStructure);

  let oEquivalentMoleculeInput = {
    MoleFraction: oMoleFractionValues,
    AtomicStructure: oAtomicStructure
  }
  let oEquivalentMolecule = FuelCalculator.Fuels.calculateEquivalentMolecule(oEquivalentMoleculeInput);
  let iMolecularWeight = FuelCalculator.Fuels.calculateMolecularWeight(oEquivalentMolecule);
  
  let oBondEnergiesKmol = oBondEnergyTableBuffer.bondEnergyPerMolecule;
  let oBondEnergiesKg = {};
  let iBondEnergiesSum = 0;

  for(var i = 1; i < 7; i++) {
     let iTotalBondEnergy = FuelCalculator.Fuels.getOrDefaultNumber(oBondEnergiesKmol["bond"+i] / oMolecularWeight["bond"+i]);
     if(!isFinite(iTotalBondEnergy)) iTotalBondEnergy = 0;

     oBondEnergiesKg["bond"+i] = iTotalBondEnergy;
     iBondEnergiesSum += iTotalBondEnergy;
  }

  oBondEnergiesKg["total"] = FuelCalculator.Fuels.getOrDefaultNumber(oBondEnergiesKmol.total / iMolecularWeight);

  return oBondEnergiesKg;
}

function getBondEnergyByTypeJSON() {
  return {
    c1c: 0,
    c2c: 0,
    c3c: 0,
    c1h: 0,
    c1n: 0,
    c2n: 0,
    c3n: 0,
    c1o: 0,
    c2o: 0,
    c3o: 0,
    h1h: 0,
    n1h: 0,
    n1n: 0,
    n2n: 0,
    n3n: 0,
    n1o: 0,
    n2o: 0,
    o1h: 0,
    o1o: 0,
    o2o: 0
  }
}
