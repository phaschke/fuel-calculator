export function getTransportationSystemUnits() {
  return {
    price: [{
        key: "USD/gal",
        display: "$/gal"
      },
      {
        key: "USD/l",
        display: "$/L"
      },
      {
        key: "USD/lb",
        display: "$/lb"
      },
      {
        key: "USD/kg",
        display: "$/kg"
      },
      {
        key: "EUR/gal",
        display: "&euro;/gal"
      },
      {
        key: "EUR/l",
        display: "&euro;/L"
      },
      {
        key: "EUR/lb",
        display: "&euro;/lb"
      },
      {
        key: "euro/kg",
        display: "&euro;/kg"
      }
    ],
    fuelEconomy: [{
        key: "mi/gal",
        display: "mile/gal"
      },
      {
        key: "km/L",
        display: "km/L"
      }
    ]
  }
}

export function getTransportationAnalysisUnits() {
  return {
    energyUsagePerDistance: [{
        key: "mj/mi",
        display: "MJ/mile"
      },
      {
        key: "mj/km",
        display: "MJ/km"
      },
      {
        key: "mmbtu/mi",
        display: "MMBTU/mile"
      },
      {
        key: "mmbtu/km",
        display: "MMBTU/km"
      }
    ],
    costPerDistance: [{
        key: "USD/mi",
        display: "$/mile"
      },
      {
        key: "USD/km",
        display: "$/km"
      },
      {
        key: "EUR/mi",
        display: "&euro;/mile"
      },
      {
        key: "EUR/km",
        display: "&euro;/km"
      }
    ],
    costPerUnitEnergy: [{
        key: "USD/mj",
        display: "$/MJ"
      },
      {
        key: "EUR/mj",
        display: "&euro;/MJ"
      },
      {
        key: "USD/mmbtu",
        display: "$/MMBTU"
      },
      {
        key: "EUR/MMBTU",
        display: "&euro;/MMBTU"
      }
    ],
    co2PerDistance: [{
        key: "lb/mi",
        display: "lb/mile"
      },
      {
        key: "lb/km",
        display: "lb/km"
      },
      {
        key: "kg/mi",
        display: "kg/mile"
      },
      {
        key: "kg/km",
        display: "kg/km"
      }
    ],
    co2PerUnitEnergy: [{
        key: "lb/mj",
        display: "lb/MJ"
      },
      {
        key: "kg/mj",
        display: "kg/MJ"
      },
      {
        key: "lb/mmbtu",
        display: "lb/MMBTU"
      },
      {
        key: "kg/mmbtu",
        display: "kg/MMBTU"
      }
    ]
  }
}

export function getDefaultTransportationAnalysisUnits() {
  return {
    energyUsagePerDistance: "mj/mi",
    costPerDistance: "USD/mi",
    costPerUnitEnergy: "USD/mj",
    co2PerDistance: "kg/mi",
    co2PerUnitEnergy: "kg/mj"
  }
}
